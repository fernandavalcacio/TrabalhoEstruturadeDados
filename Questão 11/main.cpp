#include <iostream>

using namespace std;
const int tamanho=10;
int vetor[tamanho];

void lerVetor (int vet[], int tamanho){
    for (int i=0;i<tamanho;i++){
            cout<<"Entre com a "<<(i+1)<<"posi��o:";
            cin>>vet[i];
    }
}

void bubbleSort (int vet[], int tamanho){
    for (int i+tamanho-1;i>=0;i--){
            for (int j=0;j<i;j++){
                    if (vet[j]>vet[j+1]){
                            int temp = vet[j];
                            vet [j] = vet[j+1];
                            vet[j+1]=temp;
                    }
            }
    }
}

void selectionSort (int vet[], int tamanho){
    for (int i=0;i<tamanho;i++){
            int menor=i;
            for (int j=i+1;j<tamanho;j++){
                    if (vet[menor]>vet[j]){
                            menor=j;
                    }
            }
            if (i!=menor){
                    int temp = vet[i];
                    vet[i] = vet[menor];
                    vet[menor] = temp;
            }
    }}

void insertionSort (int vet[], int tamanho){
    for (int i=1;i<tamanho;i++){
            int ref=vet[i];
            int j = i-1;
            while (j>=0&&ref<vet[j]){
                    vet[j+1]=vet[j];
                    j--;
            }

            vet[j+1]=ref;
    }
}

void ordenarVetor (int opcao, int tamanho){
    cout<<"\n1.Selection Sort"<<endl;
    cout<<"2.Insertion Sort"<<endl;
    cout<<"3.Bubble Sort"<<endl;
    cout<<"Escolha uma op��o de ordenamento de acordo com o n�mero:";
    cin>>opcao;

    if(opcao==1){
            selectionSort(vetor,tamanho);
    }else{if(opcao==2){
        insertionSort(vetor,tamanho);
    }else{if(opcao==3){
        bubbleSort(vetor,tamanho);
    }else{
        cout<<"\nOp��o inv�lida, tente novamente"<<endl;
        ordenarVetor(opcao,tamanho);
    }
    }
    }
}

void imprimirVetor (int vet[], int tamanho){
    cout<<endl;
    for (int i=0; i<tamanho; i++){
    cout<<"Posi��o"<<(i+1)<<":";
    cout<<vet[i]<<endl;}
}

int main()
{

lerVetor(vetor,tamanho);
int alvo;
ordenarVetor(alvo,tamanho);
imprimirVetor(vetor,tamanho);

return 0;
}

