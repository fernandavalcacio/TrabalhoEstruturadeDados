#include <iostream>

using namespace std;
const int tamanho=3;
int matrizA[tamanho][tamanho], matrizB[tamanho][tamanho];

void LerMatrizes (int mA[tamanho][tamanho], int mB[tamanho][tamanho], int tam){
    cout<<"Ler a Primeira Matriz"<< endl;
    for (int i=0;i<tam;i++){
      for (int j=0;j<tam;j++){
            cout<<"Entre com o valor"<<"["<<(j+1)<<"]"<<"["<<(i+1)<<"]:";
            cin>>mA[j][i];
      }
    }

    cout<<endl<<"Ler a Segunda Matriz"<< endl;
      for (int i=0;i<tam;i++){
        for (int j=0;j<tam;j++){
            cout<<"Entre com o valor"<<"["<<(j+1)<<"]"<<"["<<(i+1)<<"]:";
            cin>>mB[j][i];
        }
      }
}

void SomarMatrizes (int mA[tamanho][tamanho], int mB[tamanho][tamanho], int tam){
    for (int i=0;i<tam;i++){
      for (int j=0;j<tam;j++){
            cout<< "Soma da primeira matriz"<<"["<<(j+1)<<"]"<<"["<<(i+1)"] com a segunda matriz "<<"["<<(j+1)<<"]"<<"["<<(i+1)<<"]:";
            cout<< mA[j][i] + mB[j][i]<< endl;
      }
    }
}

void MultiplicarMatrizes (int mA[tamanho][tamanho], int mB[tamanho][tamanho], int tam){
    cout<<endl;
    for (int i=0;i<tam;i++){
      for(int j=0;j<tam;j++){
            cout<<"Multiplicacao da primeira matriz"<<"["<<(j+1)<<"]"<<"["<<(i+1)<<"]com a segunda matriz "<<"["<<(j+1)<<"]"<<"["<<(i+1)<<']:";
            cout << mA[j][i] * mB[j][i]<<endl;
      }
    }
}

int main()
{
    LerMatrizes(matrizA,matrizB,tamanho);
    SomarMatrizes(matrizA,matrizB,tamanho);
    MultiplicarMatrizes(matrizA,matrizB,tamanho);

return 0;
}
